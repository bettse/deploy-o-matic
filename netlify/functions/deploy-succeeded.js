const fetch = require("node-fetch");

const empty = {
  statusCode: 204,
};

const base = 'https://sankyo.ericbetts.dev';

async function disableShutter() {
  console.log('disableShutter');
  return fetch(`${base}/shutter`, {
    method: "POST",
    body: JSON.stringify({enabled: false}),
  });
}

async function move(action) {
  console.log('move', {action});
  return fetch(`${base}/move`, {
    method: "POST",
    body: JSON.stringify({action}),
  });
}

async function magstripe(track, content) {
  console.log('magstripe', {track, content});
  return fetch(`${base}/magstripe`, {
    method: "POST",
    body: JSON.stringify({track, content}),
  });
}

async function encodeCard(id, when) {
  // capture any card already in the sankyo
  await move('capture');
  // disable shutter to allow us to load from hopper
  await disableShutter();
  // load
  await move('load');
  // write
  await magstripe(1, id.toUpperCase());
  // write
  await magstripe(2, when);
  // eject
  await move('eject');
}

exports.handler = async (event, context) => {
  const { httpMethod } = event;
  if (httpMethod !== 'POST') {
    return empty;
  }
  try {
    const body = JSON.parse(event.body);
    const { payload } = body;
    const { id, published_at, context } = payload;
    if (context !== "production") {
      return empty;
    }

    const when = Math.ceil(Date.parse(published_at) / 1000);
    await encodeCard(id, when);

    return {
      statusCode: 200,
      body: JSON.stringify({
        track1: id,
        track2: when,
      }),
      headers: {
        "Content-Type": "application/json"
      }
    };
  } catch (error) {
    console.log({error});
  }

  return {
    statusCode: 401,
    body: JSON.stringify({error: 'Unauthorized'}),
    headers: {
      "Content-Type": "application/json"
    }
  };
};
