
import Container from 'react-bootstrap/Container';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ResponsiveEmbed from 'react-bootstrap/ResponsiveEmbed';

import './App.css';

function App() {
  return (
    <div className="App">
      <Container className="text-center pt-5" fluid='md'>
        <Jumbotron>
          <h1>The Amazing Deploy-o-Matic</h1>
          <ResponsiveEmbed aspectRatio="16by9">

            <video controls>
              <source src="/deploy-o-matic.mp4" type="video/mp4" />
              {`Sorry, your browser doesn't support embedded videos.`}
            </video>
          </ResponsiveEmbed>
              <h3>Every deploy of this site will have its <code>id</code> and <code>published_at</code> date encoded onto the magstripe of a card</h3>
        </Jumbotron>
			</Container>
      <footer className="footer font-small mx-auto pt-5">
        <Container fluid className="text-center">
          <Row noGutters>
            <Col>
              This uses a Netlify function called "deploy-succeeded" which is automatically called on each deploy to send commands to an ESP32 connected to a Sankyo SCT3Q8.
            </Col>
          </Row>
        </Container>
      </footer>
    </div>
  );
}

export default App;
